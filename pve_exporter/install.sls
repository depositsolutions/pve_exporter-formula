# -*- coding: utf-8 -*-
# vim: ft=sls
{% from "pve_exporter/map.jinja" import pve_exporter with context %}

pve_exporter-create-user:
  user.present:
    - name: {{ pve_exporter.user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

pve_exporter-create-group:
  group.present:
    - name: {{ pve_exporter.group }}
    - members:
      - {{ pve_exporter.user }}
    - require:
      - user: {{ pve_exporter.user }}

pve_exporter-config-dir:
  file.directory:
    - name: {{ pve_exporter.config_dir }}
    - makedirs: True
    - user: {{ pve_exporter.user }}
    - group: {{ pve_exporter.group }}
    - mode: 0750
    - require:
      - user: {{ pve_exporter.user }}
      - group: {{ pve_exporter.group }}


python-pip:
  pkg.installed

pve_exporter-install-binary:
  pip.installed:
    - name: prometheus-pve-exporter

pve_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/pve_exporter.service
    - source: salt://pve_exporter/files/pve_exporter.service.j2
    - template: jinja
    - context:
        pve_exporter: {{ pve_exporter }}
  service.running:
    - name: pve_exporter
    - enable: True
    - start: True

pve_exporter-service-reload:
  service.running:
    - name: pve_exporter
    - enable: True
    - watch:
      - file: pve_exporter-install-service
